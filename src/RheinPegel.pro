#-------------------------------------------------
#
# Project created by QtCreator 2019-10-04T09:30:01
#
#-------------------------------------------------

QT       += core gui network widgets

TARGET = rheinpegel
TEMPLATE = app
DEFINES += QT_DEPRECATED_WARNINGS
CONFIG += c++11

CONFIG(release, debug|release){
    message("Build RELEASE")
    DEFINES +=  QT_NO_DEBUG_OUTPUT
}else{
    message("Build DEBUG")
    DEFINES += DEBUGCYCLE
}


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    pegelonline.cpp \
    canvas.cpp \
    configdialog.cpp

HEADERS += \
        mainwindow.h \
    pegelonline.h \
    canvas.h \
    configdialog.h

FORMS += \
        mainwindow.ui \
    configdialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc \
    mytheme.qrc

TRANSLATIONS += rheinpegel_de_DE.ts

DISTFILES += \
    rheinpegel.qmodel
