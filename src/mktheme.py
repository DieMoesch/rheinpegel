#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: set fileencoding=utf-8 filetype=python syntax=python expandtab tabstop=8 softtabstop=4 shiftwidth=4:
#
# Generate tango.qrc

import os
import xml.etree.ElementTree as etree
import xml.dom.minidom as minidom
import configparser

if __name__ == '__main__':

    parser = configparser.ConfigParser()
    parser.read('mytheme/index.theme')

    theme_name = parser.get('Icon Theme', 'Name')
    dirs = parser.get('Icon Theme', 'Directories').split(',')

    print("Theme: {} Dirs: {}".format(theme_name, dirs))

    root = etree.Element('RCC', version='1.0')
    element_qresource = etree.SubElement(root, 'qresource', prefix='icons/%s' % theme_name)

    element = etree.SubElement(element_qresource, 'file', alias='index.theme')
    element.text = 'mytheme/index.theme'

    for fld in dirs:
        for basefld, _, files in os.walk("mytheme/"+fld):
            for name in files:
                icon_alias = os.path.join(fld, name)
                icon_path = os.path.join(basefld, name)
                element = etree.SubElement(element_qresource, 'file', alias=icon_alias)
                element.text = icon_path

    with open(theme_name + ".qrc", 'w') as fp:
        fp.write(minidom.parseString(etree.tostring(root)).toprettyxml())
# EOF
