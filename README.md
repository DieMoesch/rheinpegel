# RheinPegel

Show current level of river Rhein (Germany).
This QT Application requests data from [PegelOnline](https://www.pegelonline.wsv.de/).

Licence: GPL

![Screenshot](img/Screenshot.png "MainWindow")

# Required

 * C++
 * QT5

# Linux

## Debug Build

```
mkdir Debug
cd Debug
qmake CONFIG+=debug ../src
make
```

## Debian Package

```
cd src
dpkg-buildpackage -uc -us -tc
```
# Windows

Can complied. But I have no development system to test. I think, some icons will missed.
