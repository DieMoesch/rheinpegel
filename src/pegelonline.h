/*
 *
 *
 * See:
 *  http://www.pegelonline.wsv.de/webservice/guideRestapi
 *  http://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?waters=RHEIN
 *  
 *  Köln => a6ee8177-107b-47dd-bcfd-30960ccc6e9c
 *
 *  http://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/a6ee8177-107b-47dd-bcfd-30960ccc6e9c/W/currentmeasurement.json
 *  http://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/a6ee8177-107b-47dd-bcfd-30960ccc6e9c/W/measurements.json?start=P15D
 */


#ifndef PEGELONLINE_H
#define PEGELONLINE_H

#include <QWidget>
#include <QDebug>

#include <QSettings>

#include <QNetworkAccessManager>
#include <QNetworkReply>

#include <QDateTime>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

class Pegel
{
    public:
        double water=0.0;     // In cm
        QDateTime timestamp;
};

class Station
{
    public:
      QString name = "";  // ex KÖLN
      QString uuid = "";  // ex 1234-1234-1223-11233
};

class PegelOnline : public QWidget
{
    Q_OBJECT

private:
    Station* station;  // ex KÖLN

    Pegel *current = nullptr;

    QNetworkAccessManager *nm;
    QNetworkReply *reply_data;
    QNetworkReply *reply_stations;

    QVector<Pegel*> history;
    QDateTime oldest;

    QVector<Station*> stations;

public:
    explicit PegelOnline(QWidget *parent = nullptr);
    ~PegelOnline();

    Pegel* getCurrent();
    QVector<Pegel*> getHistory();
    QDateTime getOldest();

    QVector<Station*> getStaions();
    Station* getStaion();

signals:
    void updated();

public slots:
    void update_data();        // Request for update data
    void read_data_reply();    // Response water from PegelOnline
    void read_stations_reply(); // Response stations from PegelOnline
};

#endif // PEGELONLINE_H
