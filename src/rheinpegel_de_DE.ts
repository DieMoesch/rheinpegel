<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de">
<context>
    <name>ConfigDialog</name>
    <message>
        <location filename="configdialog.ui" line="14"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="26"/>
        <location filename="configdialog.ui" line="45"/>
        <location filename="configdialog.ui" line="51"/>
        <location filename="configdialog.ui" line="61"/>
        <location filename="configdialog.ui" line="83"/>
        <location filename="configdialog.ui" line="89"/>
        <location filename="configdialog.ui" line="99"/>
        <location filename="configdialog.ui" line="121"/>
        <location filename="configdialog.ui" line="127"/>
        <location filename="configdialog.ui" line="143"/>
        <location filename="configdialog.ui" line="165"/>
        <source>Keep Rheinpegel running. Only hide Window on close.</source>
        <translation>Programm nicht beenden, nur unsichtbar machen.</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="54"/>
        <source>Maximum Level</source>
        <oldsource>Maximum Pegel</oldsource>
        <translation>Maximaler Pegel</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="67"/>
        <source>11</source>
        <translation></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="92"/>
        <source>Normal Level</source>
        <oldsource>Normal Pegel</oldsource>
        <translation>Normaler Pegel</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="105"/>
        <source>3.5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="130"/>
        <source>Station</source>
        <translation>Station</translation>
    </message>
    <message>
        <location filename="configdialog.ui" line="168"/>
        <source>Stay Systray</source>
        <translation>In Taskleiste</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="26"/>
        <source>RheinLevel</source>
        <oldsource>RheinPegel</oldsource>
        <translation>RheinPegel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="55"/>
        <source>Level</source>
        <oldsource>Pegel</oldsource>
        <translation>Pegel</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="67"/>
        <source>???</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="90"/>
        <source>00.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="108"/>
        <source>m</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="115"/>
        <source>1.1.1970</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="159"/>
        <source>toolBar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="189"/>
        <source>Refresh</source>
        <translatorcomment>Neu Laden</translatorcomment>
        <translation>Neu Laden</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="192"/>
        <source>Refresh Data</source>
        <translation>Daten aktualisieren</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="195"/>
        <source>F5</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="204"/>
        <location filename="mainwindow.ui" line="207"/>
        <source>Quit</source>
        <translation>Ende</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="219"/>
        <location filename="mainwindow.ui" line="222"/>
        <source>Configuration</source>
        <translation>Konfiguration</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="225"/>
        <source>F10</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="41"/>
        <source>&amp;Quit</source>
        <translation>Ende</translation>
    </message>
</context>
</TS>
