#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QMenu>

#include "pegelonline.h"
#include "canvas.h"
#include "configdialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow() override;

public slots:
    void update_request();
    void refresh_data();
    void config();
    void trayClick(QSystemTrayIcon::ActivationReason reason);

protected:
    void closeEvent(QCloseEvent *event) override;

private:
    Ui::MainWindow *ui;

    QSystemTrayIcon* tray;

    Canvas* can;
    PegelOnline* po;
    ConfigDialog* cd = nullptr;

    bool getSettingsSystray();
};

#endif // MAINWINDOW_H
