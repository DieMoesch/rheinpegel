/*
 *
 */

#include "pegelonline.h"


/**
 * @brief PegelOnline::PegelOnline
 * @param parent
 */
PegelOnline::PegelOnline(QWidget *parent) : QWidget(parent)
{
  this->oldest = QDateTime::currentDateTime();
  nm = new QNetworkAccessManager(this);

  this->station = nullptr;

  //stations.push_back(station);
}

PegelOnline::~PegelOnline()
{
    // Clear old data (also HEAP)
    while( !history.isEmpty()) {
      Pegel* p = history.takeLast();
      delete p;
    }

    // Clear old data (also HEAP)
    while( !stations.isEmpty()) {
      Station* s = stations.takeLast();
      delete s;
    }
}

/**
 *
 * @brief PegelOnline::update_data
 */
void PegelOnline::update_data()
{
  QSettings settings(this);

  // Default is KÖLN
  QString saved_uuid = settings.value("Station","a6ee8177-107b-47dd-bcfd-30960ccc6e9c").toString();

  qDebug() << "Request data update for " << saved_uuid;

  QUrl currentmesure("http://www.pegelonline.wsv.de/webservices/rest-api/v2/stations/" +
                     saved_uuid + "/W/measurements.json?start=P15D");

  reply_data = nm->get(QNetworkRequest(currentmesure));
  connect(reply_data, &QNetworkReply::finished, this, &PegelOnline::read_data_reply);

  QUrl url_stations("http://www.pegelonline.wsv.de/webservices/rest-api/v2/stations.json?waters=RHEIN");
  reply_stations = nm->get(QNetworkRequest(url_stations));
  connect(reply_stations, &QNetworkReply::finished, this, &PegelOnline::read_stations_reply);
}


/**
 * @brief PegelOnline::read_data_reply
 */
void PegelOnline::read_data_reply()
{
  if( reply_data->error() != QNetworkReply::NoError) {
    qCritical("Error while data response");
    qDebug() << reply_data->errorString();
    emit updated(); // Reenable Refreshbutton
    return;
  }

  qDebug() << "Read data response";
  QJsonParseError error;
  QJsonDocument jdoc = QJsonDocument::fromJson(reply_data->readAll(), &error);

  if(jdoc.isEmpty() || jdoc.isNull()) {
    qCritical() << "Parsing Json Error " << error.errorString();

  } else if(jdoc.isArray()) {

    // Clear old data (also HEAP)
    while( !history.isEmpty()) {
      Pegel* p = history.takeLast();
      delete p;
    }

    /*
          [
             {
              "timestamp": "2019-10-08T07:45:00+02:00",
               "value": 206.0
             },
             {
               "timestamp": "2019-10-08T08:00:00+02:00",
               "value": 205.0
             }
           ]
       */
    for(QJsonValue av : jdoc.array()) {
      QJsonObject o = av.toObject();

      Pegel* p = new Pegel();
      p->water = o.value("value").toDouble();
      p->timestamp = QDateTime::fromString(o.value("timestamp").toString(), Qt::ISODate );

      if(this->oldest.operator>(p->timestamp))
        this->oldest = p->timestamp;

      history.push_back(p);
      current = p;
    }

    qDebug() << "Current Pegel: " << current->water;
    qDebug() << "Current Station: " << this->station->name;
    qDebug() << "Oldest Date: " << this->oldest;

  } else {
    qWarning("No valid data in response found");
    qDebug() << jdoc;
  }

  emit updated();
}

/**
 * Read Stations Response
 * @brief PegelOnline::read_stations_reply
 */
void PegelOnline::read_stations_reply()
{

  if( reply_stations->error() != QNetworkReply::NoError) {
    qCritical("Error while stations response");
    qDebug() << reply_stations->errorString();
    return;
  }

  QSettings settings(this);
  QString saved_uuid = settings.value("Station","a6ee8177-107b-47dd-bcfd-30960ccc6e9c").toString();

  qDebug() << "Read station response";
  QJsonParseError error;
  QJsonDocument jdoc = QJsonDocument::fromJson(reply_stations->readAll(), &error);

  if(jdoc.isEmpty() || jdoc.isNull()) {
    qCritical() << "Parsing Json Error " << error.errorString();

  } else if(jdoc.isArray()) {

    // Clear old data (also HEAP)
    while( !stations.isEmpty()) {
      Station* s = stations.takeLast();
      delete s;
    }

    for ( QJsonValue item : jdoc.array()) {
      QJsonObject o = item.toObject();

      QString name = o.value("longname").toString();
      QString uuid = o.value("uuid").toString();
      qDebug() << "Got " << name << " with id " << uuid;


      Station* s = new Station();
      s->name = name;
      s->uuid = uuid;

      if(saved_uuid == uuid) {
        this->station = s;
        qDebug() << "Current Station is " << this->station->name << " " << this->station->uuid;
      }

      stations.push_back(s);
    }
  } else {
    qWarning("No Station list found in response");
  }

  qDebug() << "Count of stations is " << stations.size();
}

/*
 *
 */
Pegel* PegelOnline::getCurrent()
{
  return this->current;
}

/*
 *
 */
QVector<Pegel*> PegelOnline::getHistory()
{
  return this->history;
}

/*
 *
 */
QDateTime PegelOnline::getOldest()
{
  return this->oldest;
}

/*
 *
 */
QVector<Station*> PegelOnline::getStaions()
{
  return this->stations;
}

/*
 *
 */
Station* PegelOnline::getStaion()
{
  return this->station;
}
