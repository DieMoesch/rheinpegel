#include "canvas.h"

Canvas::Canvas(QWidget *parent, PegelOnline *po) : QWidget(parent)
{
  this->po = po;
}

void Canvas::paintEvent(QPaintEvent* event)
  {
  QSettings settings(this);
  maxmeter = settings.value("MaxPegel","11").toInt();

  // Fix wrong maxmeter
  if(maxmeter<=0 || maxmeter >20)
    maxmeter = 10;

  QRect rect = event->rect();
  QPainter painter(this);
  QDateTime now = QDateTime::currentDateTime();

  // BG
  painter.fillRect(rect, Qt::white);

  QRect area(rect.x()+20, rect.y()+5, rect.width()-5, rect.height()-20);

  if( area.width() <= oldestdays*2 || area.height() <=maxmeter*2) {
    qDebug() << "Area to small " << area;
    return;
  }

  // Axis
  painter.setPen(Qt::black);

  QFont axisFont("Helvetica", 10, QFont::Bold);
  painter.setFont(axisFont);

  painter.drawLine(area.x(),area.y() + area.height(),area.width(),area.y() + area.height());
  painter.drawLine(area.x(),area.y(),area.x(),area.y() + area.height());

  // x-axis
  int x_step = area.width()/oldestdays;
  for(int i=0; i<oldestdays; i++) {

    int x = area.x() + x_step * i;
    int y = area.y() + area.height();

    QDateTime d = now.addDays(-15+i);

    painter.drawLine(x, y-3, x, y+3);
    if(i%2 ==0)
      painter.drawText(x-20,y+14, d.toString("dd.MM"));
  }

  // y -axis
  double y_step = area.height()*1.0/maxmeter;
  for( int i=0; i<maxmeter; i++) {

    int x = area.x();
    int y =  static_cast<int>(area.y() + y_step*i);
    painter.drawLine(x-3, y, x+3, y);
    painter.drawText(x-19,y+5,QString::number(maxmeter-i));
  }

  // Normal Line
  double normalmeter = settings.value("NormalPegel","3.5").toDouble();
  if( normalmeter > 0.0 && normalmeter < maxmeter*1.0)  {
    int water_y =  std::min(static_cast<int>( normalmeter * area.height() / maxmeter), area.height());

    int ny = area.y()+ area.height() - water_y;
    painter.setPen(Qt::green);
    painter.drawLine(area.x(), ny, area.width(), ny);
  }

  // Draw data

  assert (po!=nullptr);

  QDateTime oldest = po->getOldest();
  qint64 axissec = oldest.secsTo(now);

  painter.setPen(Qt::blue);
  for( Pegel* p : po->getHistory()) {
      qint64 point_sec = p->timestamp.secsTo(now);

      int water_x = static_cast<int>(area.width() * point_sec / axissec);
      int point_x = area.x() + area.width() - water_x;

      int water_y =  std::min(static_cast<int>( p->water * area.height() / maxmeter / 100), area.height());
      int point_y = area.y() + area.height() - water_y;

      painter.drawPoint(point_x, point_y);
  }
}
