#include "configdialog.h"
#include "ui_configdialog.h"
#include "pegelonline.h"

#include <QCloseEvent>

ConfigDialog::ConfigDialog(QWidget *parent, PegelOnline* po) :
  QDialog(parent),
  ui(new Ui::ConfigDialog)
{
  ui->setupUi(this);
  QSettings settings(this);

  QString maxpegel = settings.value("MaxPegel", "11.0").toString();
  ui->maxEdit->setText(maxpegel);

  QString normalpegel = settings.value("NormalPegel", "3.5").toString();
  ui->normalEdit->setText(normalpegel);

  QString station_uuid = settings.value("Station").toString();

  for(Station* s : po->getStaions())
  {
     ui->comboBox->addItem(s->name, s->uuid);
     if( s->uuid == station_uuid)
       ui->comboBox->setCurrentText(s->name);
  }

  bool systray = settings.value("Systray", "False").toBool();
  if(systray) {
    ui->systrayCheckBox->setCheckState(Qt::Checked);
  } else {
    ui->systrayCheckBox->setCheckState(Qt::Unchecked);
  }
}

ConfigDialog::~ConfigDialog()
{
  delete ui;
}

void ConfigDialog::accept()
{
  QSettings settings(this);

  // Station
  settings.setValue("Station", ui->comboBox->currentData().toString());

  // Normal and Max
  settings.setValue("MaxPegel", ui->maxEdit->text());
  settings.setValue("NormalPegel", ui->normalEdit->text());

  // Systray
  if(ui->systrayCheckBox->isChecked()) {
    settings.setValue("Systray", "True");
  } else {
    settings.setValue("Systray", "False");
  }

  emit configChanged();
  this->hide();

}
