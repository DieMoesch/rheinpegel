#include "mainwindow.h"
#include "configdialog.h"
#include "canvas.h"

#include "ui_mainwindow.h"


/**
 * @brief MainWindow::MainWindow
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  po = new PegelOnline(this);

  can = new Canvas(parent, po);
  ui->graph_layout->addWidget(can);

  // Toolbar Actions
  QObject::connect(ui->actionRefresh, &QAction::triggered, this, &MainWindow::update_request);
  QObject::connect(ui->actionConfiguration, &QAction::triggered, this, &MainWindow::config);
  QObject::connect(ui->actionQuit, &QAction::triggered, qApp, &QCoreApplication::quit);

  // Refresh Window when new data received
  QObject::connect(po,  &PegelOnline::updated, this, &MainWindow::refresh_data);

  // Tray Menu
  QIcon icon = QIcon(":/icons/pegel48.png");
  tray = new QSystemTrayIcon(this);

  QMenu *trayMenu = new QMenu(this);

  QAction *quitAction = new QAction(tr("&Quit"), this);
  trayMenu->addAction(quitAction);
  QObject::connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

  tray->setContextMenu(trayMenu);
  tray->setIcon(icon);
  QObject::connect(tray, &QSystemTrayIcon::activated, this, &MainWindow::trayClick);

  emit this->update_request();
}

/**
 * @brief MainWindow::~MainWindow
 */
MainWindow::~MainWindow()
{
  delete po;
  delete ui;
}


/*
 * Systray enable?
 */
bool MainWindow::getSettingsSystray() {

  QSettings settings(this);
  bool systray = settings.value("Systray","false").toBool();

  return systray;
}


/*
 * Userclick on systray icon
 *
 * @brief MainWindow::trayClick
 * @param reason
 */
void MainWindow::trayClick(QSystemTrayIcon::ActivationReason reason){
  if(reason == QSystemTrayIcon::Trigger) {
      if( isVisible()) {
          hide();
        } else {
          setVisible(true);
        }
    }
}


/*
 * Window close event. Hide or close window
 */
void MainWindow::closeEvent(QCloseEvent *event){

  if(this->getSettingsSystray()) {

      tray->setVisible(true);

      hide();
      event->ignore();

    } else {
      tray->setVisible(false);
    }
}


/*
 * Refresh request by by user or time
 */
void MainWindow::update_request()
{
  ui->actionRefresh->setEnabled(false);

  if(this->getSettingsSystray()) {
      tray->setVisible(true);
    } else {
      tray->setVisible(false);
    }

  emit po->update_data();
}

/*
 * New data available. Redraw mainwindow
 */
void MainWindow::refresh_data()
{
  Pegel* current = po->getCurrent();

  if(current != nullptr) {
      qDebug() << "Draw data " << po->getStaion()->name << " Current: " << current->water;

      // Station name
      ui->label_name->setText(po->getStaion()->name);

      // Meter
      double pegel_m = current->water/100.0;
      QString str;
      str.sprintf("%.2f", pegel_m);
      ui->label_meter->setText(str);

      // Timestamp
      QString ts = current->timestamp.toString(Qt::LocalDate);
      ui->label_time->setText(ts);

      // Repaint canvas
      can->repaint();
    }
  ui->actionRefresh->setEnabled(true);
}


/*
 * Show config dialig
 */
void MainWindow::config()
{
  if(cd==nullptr) {
      cd = new ConfigDialog(this, po);
      connect(cd, &ConfigDialog::configChanged, this, &MainWindow::update_request);
    }

  cd->setVisible(true);
}
