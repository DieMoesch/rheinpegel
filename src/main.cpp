/*
 *
 */

#include <iostream>
#include <QApplication>
#include <QtGlobal>
#include <QTimer>
#include <QObject>
#include <QTranslator>

#include "mainwindow.h"

#ifdef DEBUGCYCLE
#define CYCLETIME 10000
#else
#define CYCLETIME 15*60000
#endif

void msgHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
  QString output;
  switch(type) {

    case QtDebugMsg:
      output = QString("Debug %1 %2").arg(context.file).arg(msg);
      break;

    case QtInfoMsg:
      output = QString("Info %1 %2").arg(context.file).arg(msg);
      break;

    case QtWarningMsg:
      output = QString("Warnig %1 %2").arg(context.file).arg(msg);
      break;

    case QtCriticalMsg:
      output = QString("Critical %1 %2").arg(context.file).arg(msg);
      break;

    case QtFatalMsg:
      output = QString("Fatal %1 %2").arg(context.file).arg(msg);
      break;
    }
  std::cerr << output.toStdString() << std::endl;

  if( type == QtFatalMsg ) abort();
}

int main(int argc, char *argv[])
{
  qInstallMessageHandler(msgHandler);
  QApplication app(argc, argv);

  // Settings
  app.setOrganizationName("DieMoesch");
  app.setApplicationName("Rheinpegel");

  // Translation
  QTranslator translator;
  QString locale = QLocale::system().name();
  translator.load(QString("rheinpegel_") + locale, QString(":/i18n"));
  qDebug() << "Translate by " << QString("rheinpegel_") + locale;
  app.installTranslator(&translator);

  MainWindow w;
  w.show();

  // Updatetimer 5 minutes
  qDebug() << "Cycletime: " << CYCLETIME;
  QTimer* timer = new QTimer(&w);
  QObject::connect (timer, &QTimer::timeout, &w, &MainWindow::update_request );
  timer->start (CYCLETIME);

  return app.exec();
}
