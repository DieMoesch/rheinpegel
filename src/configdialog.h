#ifndef CONFIGDIALOG_H
#define CONFIGDIALOG_H

#include <QDialog>
#include <QDialogButtonBox>
#include <QSettings>

#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonArray>

#include "pegelonline.h"

namespace Ui {
  class ConfigDialog;
}

class ConfigDialog : public QDialog
{
  Q_OBJECT

public:
  explicit ConfigDialog(QWidget *parent = nullptr, PegelOnline* po = nullptr);
  ~ConfigDialog() override;

signals:
  void configChanged();

protected:
  void accept() override;

private:
  Ui::ConfigDialog* ui;
  QNetworkAccessManager* nm;
  QNetworkReply* reply;
  PegelOnline* po;

};

#endif // CONFIGDIALOG_H
