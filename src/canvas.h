#ifndef CANVAS_H
#define CANVAS_H

#include <assert.h>
#include <QWidget>
#include <QDebug>
#include <QPainter>
#include <QPaintEvent>
#include <QFont>

#include "pegelonline.h"


class Canvas : public QWidget
{
  Q_OBJECT


public:
  explicit Canvas(QWidget *parent = nullptr, PegelOnline *po=nullptr);

private:
  void paintEvent(QPaintEvent*);
  PegelOnline *po;

  int maxmeter = 6;
  int oldestdays = 15;

signals:

public slots:
};

#endif // CANVAS_H
